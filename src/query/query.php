<?php

namespace rongon\query;
use PDO;
use rongon\Db\Db;

class Query extends Db
{
    public function allPost(){
        $sql = "SELECT * FROM `tbl_post`";
        $query = $this->dbh->prepare($sql);
        $query->execute();
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }
    public function latest(){
        $sql = "SELECT * FROM `tbl_post` LIMIT 4";
        $query = $this->dbh->prepare($sql);
        $query->execute();
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }
    public function time_formate($date){
        return date('M d, Y , h:m A',strtotime($date));
    }
    public function textShort($text,$limit = 400){
        $text=$text." ";
        $text=substr($text,0,$limit);
        $text=$text.".....";
        return $text;
    }
    public function showPost($id){
        $sql = "SELECT * FROM `tbl_post` WHERE `id`=$id";
        $query = $this->dbh->prepare($sql);
        $query->execute();
        return $query->fetch(PDO::FETCH_ASSOC);

    }
    public function related_post($value){
        $sql = "SELECT * FROM `tbl_post` WHERE `cat`=$value ORDER by rand() limit 6" ;
        $query = $this->dbh->prepare($sql);
        $query->execute();
        return $query->fetchAll(PDO::FETCH_ASSOC);

    }
    public function scarch($value){
        $sql = "SELECT * FROM `tbl_post` WHERE `title` LIKE '%$value%' OR `body` LIKE '%$value%'";
        $query = $this->dbh->prepare($sql);
        $query->execute();
        return $query->fetchAll(PDO::FETCH_ASSOC);

    }
    public static function basic_test($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }



}