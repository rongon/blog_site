<?php

namespace rongon\query;


use rongon\Db\Db;
use PDO;

class pagination extends Db
{
    public function limit($defualt){
        if (isset($_REQUEST['page'])){
            return $_REQUEST['page'];
        }
        else{
            return $defualt;
        }
    }
    public function indexPaginator($page = 1,$itemPerpage){
        $start = ($page-1) * $itemPerpage;
        if ($start<0)$start = 0;
        $sql = "SELECT * FROM `tbl_post` LIMIT $start,$itemPerpage";
        $query = $this->dbh->prepare($sql);
        $query->execute();
        $arrorSamedata = $query->fetchAll(PDO::FETCH_ASSOC);
        return $arrorSamedata;

    }
}