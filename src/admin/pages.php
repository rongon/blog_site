<?php

namespace rongon\admin;

use PDO;
use rongon\Db\Db;

class pages extends Db
{
 public function page(){
     $sql = "SELECT * FROM `tbl_pages`";
     $query = $this->dbh->prepare($sql);
     $query->execute();
     return $query->fetchAll(PDO::FETCH_ASSOC);
 }
 public function addPage($data){
     $sql = "INSERT INTO `tbl_pages` (`id`, `name`, `body`) VALUES (NULL, :name ,:body )";
     $query = $this->dbh->prepare($sql);
     $query->bindParam("name",$data['name']);
     $query->bindParam("body",$data['body']);
     return $query->execute();
 }
}