<?php

namespace rongon\admin;
use PDO;
use rongon\Db\Db;

class socal extends Db
{
    function socal($id = 1){
        $sql = "SELECT * FROM `tbl_socal_midea` WHERE `id`=$id";
        $query = $this->dbh->prepare($sql);
        $query->execute();
        return $query->fetch(PDO::FETCH_ASSOC);

    }
    function socalUpdate($data)
    {
        $sql = "UPDATE `tbl_socal_midea` SET `facebook` = :facebook , `instragram` = :instragram WHERE `tbl_socal_midea`.`id` = 1";
        $query = $this->dbh->prepare($sql);
        $query->bindParam("facebook", $data['facebook']);
        $query->bindParam("instragram", $data['instragram']);
        return $query->execute();
    }

    function copyUpdate($data){
        $sql = "UPDATE `tbl_copyright` SET `copyright` = :copyright WHERE `tbl_copyright`.`id` = 1;";
        $query = $this->dbh->prepare($sql);
        $query->bindParam("copyright",$data['copyright']);
        return $query->execute();
    }
    function copyright($id = 1){
        $sql = "SELECT * FROM `tbl_copyright` WHERE `id`=$id";
        $query = $this->dbh->prepare($sql);
        $query->execute();
        return $query->fetch(PDO::FETCH_ASSOC);

    }
}
?>