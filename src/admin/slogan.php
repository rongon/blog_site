<?php

namespace rongon\admin;

use PDO;
use rongon\Db\Db;

class slogan extends Db
{
    public function show(){
        $sql = "SELECT * FROM `tbl_slogan` WHERE `id`=2";
        $query = $this->dbh->prepare($sql);
        $query->execute();
        return $query->fetch(PDO::FETCH_ASSOC);
    }
    public function update($data){
        $sql = "UPDATE `tbl_slogan` SET `logo` = :image ,`title` = :title, `slogan` = :slogan WHERE `tbl_slogan`.`id` = 2;";
        $query = $this->dbh->prepare($sql);
        $query->bindParam("image",$data['image']);
        $query->bindParam("title",$data['title']);
        $query->bindParam("slogan",$data['slogan']);
        return $query->execute();
    }

}