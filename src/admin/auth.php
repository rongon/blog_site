<?php

namespace rongon\admin;

use PDO;
use rongon\Db\Db;

class auth extends Db
{
    public function login($data){
        $sql = "SELECT * FROM `tbl_user` WHERE `user_name`= :user_name";
        $query = $this->dbh->prepare($sql);
        $query->bindParam('user_name',$data['user_name']);
        $query->execute();
        return $query->fetch(PDO::FETCH_ASSOC);

    }
}