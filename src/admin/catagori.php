<?php

namespace rongon\admin;

use PDO;
use rongon\Db\Db;

class catagori extends Db
{
    public function addCatagori($data){
        $sql = "INSERT INTO `tbl_category` (`id`, `name`) VALUES (NULL ,:name);";
        $query = $this->dbh->prepare($sql);
        $query->bindParam('name',$data['catname']);
        return $query->execute();
    }
    public function category(){
        $sql = "SELECT * FROM `tbl_category`";
        $query = $this->dbh->prepare($sql);
        $query->execute();
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }
    public function editCatagori($data){
        $sql = "UPDATE `tbl_category` SET `name` = :name WHERE `tbl_category`.`id` = :id";
        $query = $this->dbh->prepare($sql);
        $query->bindParam('name',$data['catname']);
        $query->bindParam('id',$data['id']);
        return $query->execute();
    }
    public function singleCatagory($id){
        $sql = "SELECT * FROM `tbl_category` WHERE `id`=$id";
        $query = $this->dbh->prepare($sql);
        $query->execute();
        return $query->fetch(PDO::FETCH_ASSOC);
    }
    public function catDelet($id){
        $sql = "DELETE FROM `tbl_category` WHERE `tbl_category`.`id` = $id";
        $query = $this->dbh->prepare($sql);
        return $query->execute();
    }
}