<?php

namespace rongon\admin;

use PDO;
use rongon\Db\Db;

class post extends Db
{
    public function addPost($data){
        $sql = "INSERT INTO `tbl_post` (`id`, `cat`, `title`, `body`, `image`, `author`, `tags`, `date`) VALUES (NULL, :catagori, :title, :text, :image, :author, :teg, CURRENT_TIMESTAMP);";
        $query = $this->dbh->prepare($sql);
        $query->bindParam("catagori",$data['catagori']);
        $query->bindParam("title",$data['title']);
        $query->bindParam("text",$data['text']);
        $query->bindParam("image",$data['image']);
        $query->bindParam("author",$data['author']);
        $query->bindParam("teg",$data['teg']);
        return $query->execute();

    }
    public function postList(){
        $sql = "SELECT `tbl_post`.*,`tbl_category`.`name` FROM `tbl_post` LEFT JOIN `tbl_category` ON `tbl_post`.`cat`=`tbl_category`.`cat`";
        $query = $this->dbh->prepare($sql);
        $query->execute();
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    public function postDelet($id){
        $sql = "DELETE FROM `tbl_post` WHERE `tbl_post`.`id` = $id";
        $query = $this->dbh->prepare($sql);
        return $query->execute();
    }

    public function singlePost($id){
        $sql = "SELECT * FROM `tbl_post` WHERE `id`=$id";
        $query = $this->dbh->prepare($sql);
        $query->execute();
        return $query->fetch(PDO::FETCH_ASSOC);
    }
    public function update($data){
        $sql = "UPDATE `tbl_post` SET `cat` = :catagori , `title` = :title, `body` = :text, `image` = :image ,`author` = :author, `tags` = :tags   WHERE `tbl_post`.`id` = :id";
        $query = $this->dbh->prepare($sql);
        $query->bindParam("catagori",$data['catagori']);
        $query->bindParam("title",$data['title']);
        $query->bindParam("text",$data['text']);
        $query->bindParam("image",$data['image']);
        $query->bindParam("author",$data['author']);
        $query->bindParam("tags",$data['teg']);
        $query->bindParam("id",$data['id']);
        return $query->execute();
    }
}