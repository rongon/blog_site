<?php
namespace rongon\Db;
use PDO;
use PDOException;


class Db
{
public $dbh;
public function __construct()
{
    try{
        $this->dbh = new PDO('mysql:dbname=blog_db;host=localhost','root','');
        $this->dbh->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
    }
    catch (PDOException $erorr){
        echo $erorr->getMessage();
    }


}
}