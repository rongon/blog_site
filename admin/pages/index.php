﻿<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR
    . "autoload.php";
if (!isset($_SESSION)){
    session_start();
}
if (!isset($_SESSION['permition']) || empty($_SESSION['permition'])){
    header("location:login.php");
}
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "admin" . DIRECTORY_SEPARATOR . "elements" . DIRECTORY_SEPARATOR . "header.php";
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "admin" . DIRECTORY_SEPARATOR . "elements" . DIRECTORY_SEPARATOR . "sidber.php";
?>

        <div class="grid_10">
		
            <div class="box round first grid">
                <h2> Dashbord</h2>
                <div class="block">               
                  Welcome admin panel        
                </div>
            </div>
        </div>

<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "admin" . DIRECTORY_SEPARATOR . "elements" . DIRECTORY_SEPARATOR . "footer.php";
?>