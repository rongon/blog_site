<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR
    . "autoload.php";

if (!isset($_SESSION)){
    session_start();
}
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "admin" . DIRECTORY_SEPARATOR . "elements" . DIRECTORY_SEPARATOR . "header.php";

include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "admin" . DIRECTORY_SEPARATOR . "elements" . DIRECTORY_SEPARATOR . "sidber.php";
use rongon\admin\pages;
$query = new pages();
$page = $query->page();


?>
    <div class="grid_10">
        <div class="box round first grid">
            <h2>Add New Post</h2>
            <div class="block">
                <form action="pagestore.php" method="post">
                    <table class="form">
                        <tr>
                            <td>
                                <label>Name</label>
                                <?php
                                if (!empty($_SESSION['validation']['name'])){
                                    echo "<span style='color: red'>";
                                    echo $_SESSION['validation']['name'];
                                    echo "</span>";
                                    unset($_SESSION['validation']['name']);
                                }?>
                            </td>
                            <td>
                                <input type="text" name="name" vclass="medium" />
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top; padding-top: 9px;">
                                <label>Body</label>
                                <?php
                                if (!empty($_SESSION['validation']['body'])){
                                    echo "<span style='color: red'>";
                                    echo $_SESSION['validation']['body'];
                                    echo "</span>";
                                    unset($_SESSION['validation']['body']);
                                }
                                ?>
                            </td>
                            <td>
                                <textarea class="tinymce" name="body"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <input type="submit" name="submit" Value="Save" />
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>
<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "admin" . DIRECTORY_SEPARATOR . "elements" . DIRECTORY_SEPARATOR . "footer.php";
?><?php
/**
 * Created by PhpStorm.
 * User: RB Chonchol
 * Date: 11/22/2019
 * Time: 11:06 PM
 */