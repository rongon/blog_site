<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR
    . "autoload.php";
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "admin" . DIRECTORY_SEPARATOR . "elements" . DIRECTORY_SEPARATOR . "header.php";

include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "admin" . DIRECTORY_SEPARATOR . "elements" . DIRECTORY_SEPARATOR . "sidber.php";

use rongon\admin\post;
use rongon\admin\catagori;
$cat = new catagori();
$query = new post();
$id = $_GET['id'];
$item = $query->singlePost($id);
$posts = $cat->category();


?>
    <div class="grid_10">
        <div class="box round first grid">
            <h2>Add New Post</h2>
            <div class="block">
                <form action="postUpdate.php" method="post" enctype="multipart/form-data">
                    <table class="form">
                        <tr>
                            <td>
                                <label>Title</label>
                                <input type="hidden" value="<?= $item['id'];?>" name="id">
                            </td>
                            <td>
                                <input type="text" value="<?= $item['title'];?>" name="title" placeholder="Enter Post Title..." class="medium" />
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <label>Category</label>
                            </td>
                            <td>
                                <select id="select" name="catagori">
                                    <option >Select your Category</option>
                                    <?php
                                    foreach ($posts as $post):
                                        if ($post['cat'] == $item['cat']) :
                                        ?>
                                        <option value="<?= $post['cat']?>" selected>
                                            <?= $post['name']?>
                                        </option>
                                            <?php
                                        else:
                                            ?>
                                        <option value="<?= $post['cat']?>">
                                            <?= $post['name']?>
                                        </option>
                                            <?php
                                        endif; endforeach;?>
                                </select>
                            </td>
                        <tr>
                            <td>
                                <label>Upload Image</label>
                            </td>
                            <td>
                                <input type="file"  name="image"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Author</label>
                            </td>
                            <td>
                                <input type="text" value="<?= $item['author']?>" name="author" placeholder="Enter author name......" class="medium" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Teg</label>
                            </td>
                            <td>
                                <input type="text" value="<?= $item['tags']?>" name="teg" placeholder="Enter post teg......" class="medium" />
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top; padding-top: 9px;">
                                <label>Content</label>
                            </td>
                            <td>
                                <textarea class="tinymce" value="" name="text"><?= $item['body'];?></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <img src="data:image;base64,<?= $item['image']?>" alt="">

                            </td>
                            <td>
                                <input type="submit" name="submit" Value="Save" />
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>
<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "admin" . DIRECTORY_SEPARATOR . "elements" . DIRECTORY_SEPARATOR . "footer.php";
?>