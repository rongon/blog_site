<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR
    . "autoload.php";
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "admin" . DIRECTORY_SEPARATOR . "elements" . DIRECTORY_SEPARATOR . "header.php";

include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "admin" . DIRECTORY_SEPARATOR . "elements" . DIRECTORY_SEPARATOR . "sidber.php";

use rongon\admin\catagori;
$id = $_GET['catId'];
$query = new catagori();
$item = $query->singleCatagory($id);
$query = new catagori();
?>

<div class="grid_10">

    <div class="box round first grid">
        <h2>Edit Category</h2>
        <div class="block copyblock">
            <form action="catUpdate.php" method="post">
                <table class="form">
                    <tr>
                        <td>
                            <input type="text" value="<?= $item['name']?>" class="medium" name="catname" />
                        </td>
                    </tr>
                            <input type="hidden" value="<?= $item['id']?>" class="medium" name="id" />
                    <tr>
                        <td>
                            <input type="submit" name="submit" Value="Save" />
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>
<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "admin" . DIRECTORY_SEPARATOR . "elements" . DIRECTORY_SEPARATOR . "footer.php";
?>