﻿<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR
    . "autoload.php";
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "admin" . DIRECTORY_SEPARATOR . "elements" . DIRECTORY_SEPARATOR . "header.php";
if (!isset($_SESSION)){
    session_start();
}
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "admin" . DIRECTORY_SEPARATOR . "elements" . DIRECTORY_SEPARATOR . "sidber.php";
use rongon\admin\catagori;

$query = new catagori();
$catagoris = $query->category();
?>
<div class="grid_10">
            <div class="box round first grid">
                <h2>Category List</h2>
                <div class="block">        
                    <table class="data display datatable" id="example">
					<tbody>
                        <tr class="odd gradeX"style="font-weight: bold " >
                            <td>Serial No.</td>
                            <td>Category Name</td>
                            <td>Action</td>
                        </tr>
                            <?php
                                foreach ($catagoris as $catagori):
                            ?>
						<tr class="odd gradeX">
							<td><?= $catagori['id']?></td>
							<td><?= $catagori['name']?></td>
							<td><a href="catEdit.php?catId=<?= $catagori['id']?>">Edit</a> || <a onclick="return confirm('Are you sure to delete this sir????')" href="catDelet.php?catDel=<?= $catagori['id']?>">Delete</a></td>
						</tr>
                                <?php endforeach;?>
		            </tbody>
				</table>
               </div>
            </div>
        </div>
<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "admin" . DIRECTORY_SEPARATOR . "elements" . DIRECTORY_SEPARATOR . "footer.php";
?>