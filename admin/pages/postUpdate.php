<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR
    . "autoload.php";

use rongon\admin\post;
$query = new post();
$item = $query->singlePost($_POST['id']);
if (empty($_FILES['image']['name'])){
    $_POST['image'] = $item['image'];
}
else{
    $_POST['image'] = base64_encode(file_get_contents(addslashes($_FILES['image']['tmp_name'])));
}

$update = $query->update($_POST);

header("location:postlist.php");