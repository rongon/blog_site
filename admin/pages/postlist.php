﻿<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR
    . "autoload.php";
if (!isset($_SESSION)){
    session_start();
}
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "admin" . DIRECTORY_SEPARATOR . "elements" . DIRECTORY_SEPARATOR . "header.php";

include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "admin" . DIRECTORY_SEPARATOR . "elements" . DIRECTORY_SEPARATOR . "sidber.php";
use rongon\admin\post;
$query = new post();
$allPost = $query->postList();


?>
        <div class="grid_10">
            <div class="box round first grid">
                <h2>Post List</h2>
                <div class="block">
                 <?php
            if (!empty($_SESSION['delete'])){
                echo "<span style='color: #298017'>";
                echo $_SESSION['delete'];
                echo "</span>";
                unset($_SESSION['delete']);
            }?>
                    <?php
                    if (!empty($_SESSION['confarm'])){
                        echo "<span style='color: red'>";
                        echo $_SESSION['confarm'];
                        echo "</span>";
                        unset($_SESSION['confarm']);
                    }
                    ?>
                    <table class="data display datatable" id="example">
                    <tr style="font-weight: bold">
                        <td>Post Title</td>
                        <td>Author</td>
                        <td>Category</td>
                        <td>Action</td>
                    </tr>
                    <?php
                    foreach ($allPost as $post):
                    ?>
						<tr class="odd gradeX">
							<td><?= $post['title']; ?></td>
							<td><?= $post['author']; ?></td>
							<td><?= $post['name']; ?></td>
							<td><a href="postEdit.php?id=<?= $post['id']; ?>">Edit</a> || <a onclick="return confirm('Are you sure to delete it??????')" href="postDel.php?id=<?= $post['id']; ?>">Delete</a></td>
						</tr>
                    <?php endforeach;?>
                    </table>
               </div>
            </div>
        </div>
        <div class="clear">
        </div>
<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "admin" . DIRECTORY_SEPARATOR . "elements" . DIRECTORY_SEPARATOR . "footer.php";
?>
