<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR
    . "autoload.php";

use rongon\admin\slogan;
$query = new slogan();
$item = $query->show();
if (empty($_FILES['image']['name'])){
    $_POST['image'] = $item['logo'];
}
else{
    $_POST['image'] = base64_encode(file_get_contents(addslashes($_FILES['image']['tmp_name'])));
}

$update = $query->update($_POST);

header("location:titleslogan.php");