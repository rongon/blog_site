<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR
    . "autoload.php";
if (!isset($_SESSION)){
    session_start();
}
use rongon\query\Query;
use rongon\admin\pages;
$query = new pages();

if (empty($_POST['name'])){
    $_SESSION['validation']['name'] = 'you must enter name';
}
else{
    if (preg_match('/^[a-zA-Z ]*$/',$_POST['name'])){
        $_POST['name'] = Query::basic_test($_POST['name']);
    }
    else{
        $_SESSION['validation']['name'] = 'only alphabatic is acepted';
    }
}

if (empty($_POST['body'])){
    $_SESSION['validation']['body'] = 'you must enter body text';
}
else{
    $_POST['body'] = Query::basic_test($_POST['body']);
}




if ($query->addPage($_POST)){
    $_SESSION['success'] = 'insert page successful';
    header("location:addPage.php");
}

