﻿<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR
    . "autoload.php";
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "admin" . DIRECTORY_SEPARATOR . "elements" . DIRECTORY_SEPARATOR . "header.php";

include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "admin" . DIRECTORY_SEPARATOR . "elements" . DIRECTORY_SEPARATOR . "sidber.php";

use rongon\admin\catagori;
$query = new catagori();
$items = $query->category();



?>
<div class="grid_10">
    <div class="box round first grid">
        <h2>Add New Post</h2>
        <div class="block">
         <form action="store.php" method="post" enctype="multipart/form-data">
            <table class="form">
                <tr>
                    <td>
                        <label>Title</label>
                        <?php
                        if (!empty($_SESSION['validation']['title'])){
                            echo "<span style='color: red'>";
                            echo $_SESSION['validation']['title'];
                            echo "</span>";
                            unset($_SESSION['validation']['title']);
                        }?>
                    </td>
                    <td>
                        <input type="text" name="title" placeholder="Enter Post Title..." class="medium" />
                    </td>
                </tr>

                <tr>
                    <td>
                        <label>Category</label>
                    </td>
                    <td>
                        <select id="select" name="catagori">
                            <option >Category</option>
                            <?php
                            foreach ($items as $item):
                            ?>
                            <option value="<?= $item['cat']?>"><?= $item['name']?></option>
                            <?php endforeach;?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Upload Image</label>
                        <?php
                        if (!empty($_SESSION['validation']['image'])){
                            echo "<span style='color: red'>";
                            echo $_SESSION['validation']['image'];
                            echo "</span>";
                            unset($_SESSION['validation']['image']);
                        }
                        ?>
                    </td>
                    <td>
                        <input type="file"  name="image"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Author</label>
                        <?php
                        if (!empty($_SESSION['validation']['author'])){
                            echo "<span style='color: red'>";
                            echo $_SESSION['validation']['author'];
                            echo "</span>";
                            unset($_SESSION['validation']['author']);
                        }
                        ?>
                    </td>
                    <td>
                        <input type="text" name="author" placeholder="Enter author name......" class="medium" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Teg</label>
                        <?php
                        if (!empty($_SESSION['validation']['teg'])){
                            echo "<span style='color: red'>";
                            echo $_SESSION['validation']['teg'];
                            echo "</span>";
                            unset($_SESSION['validation']['teg']);
                        }
                        ?>
                    </td>
                    <td>
                        <input type="text" name="teg" placeholder="Enter post teg......" class="medium" />
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align: top; padding-top: 9px;">
                        <label>Content</label>
                        <?php
                        if (!empty($_SESSION['validation']['text'])){
                            echo "<span style='color: red'>";
                            echo $_SESSION['validation']['text'];
                            echo "</span>";
                            unset($_SESSION['validation']['text']);
                        }
                        ?>
                    </td>
                    <td>
                        <textarea class="tinymce" name="text"></textarea>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <input type="submit" name="submit" Value="Save" />
                    </td>
                </tr>
            </table>
            </form>
        </div>
    </div>
</div>
<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "admin" . DIRECTORY_SEPARATOR . "elements" . DIRECTORY_SEPARATOR . "footer.php";
?>