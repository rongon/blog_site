﻿<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR
    . "autoload.php";
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "admin" . DIRECTORY_SEPARATOR . "elements" . DIRECTORY_SEPARATOR . "header.php";

include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "admin" . DIRECTORY_SEPARATOR . "elements" . DIRECTORY_SEPARATOR . "sidber.php";
?>
        <div class="grid_10">

            <div class="box round first grid">
                <h2>Add New Category</h2>
               <div class="block copyblock">
                 <form action="catAdd.php" method="post">
                    <table class="form">
                        <tr>
                            <td>
                                <input type="text" placeholder="Enter Category Name..." class="medium" name="catname" />
                            </td>
                        </tr>
						<tr>
                            <td>
                                <input type="submit" name="submit" Value="Save" />
                            </td>
                        </tr>
                    </table>
                    </form>
                </div>
            </div>
        </div>
<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "admin" . DIRECTORY_SEPARATOR . "elements" . DIRECTORY_SEPARATOR . "footer.php";
?>