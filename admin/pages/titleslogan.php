﻿<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR
    . "autoload.php";
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "admin" . DIRECTORY_SEPARATOR . "elements" . DIRECTORY_SEPARATOR . "header.php";

include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "admin" . DIRECTORY_SEPARATOR . "elements" . DIRECTORY_SEPARATOR . "sidber.php";

use rongon\admin\slogan;
$query = new slogan();
$slogan = $query->show();

?>
    <style>
        .sideright{float: left;
        padding: 100px}

        .sideleft {
            float: left;
            padding: 0 122px 0 0;
        }
        .sideleft img{ height: 100px;width: 100px}

    </style>
<div class="grid_10">
    <div class="box round first grid">
        <h2>Update Site Title and Description</h2>
        <div class="sideleft">
            <div class="block sloginblock">
                <form action="slogan.php" method="post" enctype="multipart/form-data">
                    <table class="form">
                        <tr>
                            <td>
                                <label>Website Title</label>
                            </td>
                            <td>
                                <input type="text"  value="<?= $slogan['title'];?>"  name="title" class="medium" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Website Slogan</label>
                            </td>
                            <td>
                                <input type="text" value="<?= $slogan['slogan']?>" name="slogan" class="medium" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Upload logo</label>
                            </td>
                            <td>
                                <input type="file"  name="image"/>
                            </td>
                        </tr>

                        <tr>
                            <td>
                            </td>
                            <td>
                                <input type="submit" name="submit" Value="Update" />
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
        <div class="sideright">
            <level><h2>Logo</h2></level>
            <img src="data:image;base64,<?= $slogan['logo']?>" alt="logo">
        </div>
    </div>
</div>
<div class="clear">
</div>
        <?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "admin" . DIRECTORY_SEPARATOR . "elements" . DIRECTORY_SEPARATOR . "footer.php";
 ?>