<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR
    . "autoload.php";
if (!isset($_SESSION)){
    session_start();
}

use rongon\admin\post;
use rongon\query\Query;
$query = new post();

if (empty($_POST['title'])){
    $_SESSION['validation']['title'] = 'You must enter title name';
}
else{
    if (preg_match('/^[a-zA-Z ]*$/',$_POST['title'])){
        $_POST['title'] = Query::basic_test($_POST['title']);
    }
    else{
        $_SESSION['validation']['title'] = 'only alphabatic is acepted';
    }
}

if (empty($_FILES['image']['name'])){
    $_SESSION['validation']['image'] = 'you must enter image';
}
else{
    if ($_FILES['image']['type'] == ('image/jpg'||'image/png' )){
        $_POST['image'] = base64_encode(file_get_contents(addslashes($_FILES['image']['tmp_name'])));
    }
    else{
        $_SESSION['validation']['image'] = 'just Jpg and png format saported';
    }
}
if (empty($_POST['author'])){
    $_SESSION['validation']['author'] = 'You must enter author name';
}
else{
    if (preg_match('/^[a-zA-Z ]*$/',$_POST['author'])){
        $_POST['author'] = Query::basic_test($_POST['author']);
    }
    else{
        $_SESSION['validation']['author'] = 'only alphabatic is acepted';
    }
}
if (empty($_POST['teg'])){
    $_SESSION['validation']['teg'] = 'You must enter teg name';
}
else{
    if (preg_match('/^[a-zA-Z ]*$/',$_POST['teg'])){
        $_POST['teg'] = Query::basic_test($_POST['teg']);
    }
    else{
        $_SESSION['validation']['teg'] = 'only alphabatic is acepted';
    }
}
if (empty($_POST['text'])){
    $_SESSION['validation']['text'] = 'You must enter text name';
}
else{
    $_POST['text'] = Query::basic_test($_POST['text']);
}
if (empty($_SESSION['validation'])){
    $add = $query->addPost($_POST);
    $_SESSION['confarm'] = 'Data insart sussessful ';
    header("location:postlist.php");
}
else{
    header("location:addpost.php");
}
