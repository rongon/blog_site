<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR
    . "autoload.php";

if (!isset($_SESSION)){
    session_start();
}


use rongon\admin\pages;
$query = new pages();
$item = $query->page();
?>

<div class="grid_2">
    <div class="box sidemenu">
        <div class="block" id="section-menu">
            <ul class="section menu">
                <li><a class="menuitem">Site Option</a>
                    <ul class="submenu">
                        <li><a href="/admin/pages/titleslogan.php">Title & Slogan</a></li>
                        <li><a href="/admin/pages/social.php">Social Media</a></li>
                        <li><a href="/admin/pages/copyright.php">Copyright</a></li>
                    </ul>
                </li>

                <li><a class="menuitem">Update Pages</a>
                    <ul class="submenu">
                        <li><a href="/admin/pages/addPage.php">Add new Page</a> </li>
                        <?php
                        foreach ($item as $page):
                        ?>
                        <li><a><?= $page['name']?></a></li>
                        <?php  endforeach;?>
                    </ul>
                </li>
                <li><a class="menuitem">Category Option</a>
                    <ul class="submenu">
                        <li><a href="/admin/pages/addcat.php">Add Category</a> </li>
                        <li><a href="/admin/pages/catlist.php">Category List</a> </li>
                    </ul>
                </li>
                <li><a class="menuitem">Post Option</a>
                    <ul class="submenu">
                        <li><a href="/admin/pages/addpost.php">Add Post</a> </li>
                        <li><a href="/admin/pages/postlist.php">Post List</a> </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>