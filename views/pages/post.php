<?php
include_once $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR
    . "autoload.php";
include_once $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."views".DIRECTORY_SEPARATOR."elements".DIRECTORY_SEPARATOR."header.php";

use rongon\query\query;
$query = new Query();
$id = $_GET['id'];
$post =$query->showPost($id);

?>

	<div class="contentsection contemplete clear">
		<div class="maincontent clear">
			<div class="about">
				<h2><?= $post['title']?></h2>
				<h4><?=  $query->time_formate($post['date']); ?>, By <a href="#"><?= $post['author']; ?></a></h4>
                <img src="data:image;base64,<?= $post['image']; ?>" alt="">
                <?= $post['body']; ?>



				<div class="relatedpost clear">
					<h2>Related articles</h2>
                    <?php
                    $catagori = $query->related_post($post['cat']);
                    //var_dump($catagori);
                    foreach ($catagori as $catagoris) :
                    ?>
					<a href="post.php?id=<?= $catagoris['id']?>"><img src="data:image;base64,<?= $catagoris['image']?>" alt="post image"/></a>
					<?php endforeach;?>
	            </div>

		</div>

	</div>
<?php
include_once $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."views".DIRECTORY_SEPARATOR."elements".DIRECTORY_SEPARATOR."sideber.php";
?>
<?php
include_once $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."views".DIRECTORY_SEPARATOR."elements".DIRECTORY_SEPARATOR."footer.php";
?>