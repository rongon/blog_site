<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";

include_once $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."views".DIRECTORY_SEPARATOR."elements".DIRECTORY_SEPARATOR."header.php";

include_once $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."views".DIRECTORY_SEPARATOR."elements".DIRECTORY_SEPARATOR."slider.php";




use rongon\query\query;
use rongon\query\pagination;
$query = new Query();
$pagination = new pagination();
$posts =$query->allPost();

$count_number = count($posts);
$page_number = $pagination->limit(1);
$pages = ceil($count_number/3);

if ($pages >= $page_number){
    $posts = $pagination->indexPaginator($page_number,3);
}

?>

	<div class="contentsection contemplete clear">

		<div class="maincontent clear">


            <?php
            foreach ($posts as $post) : ?>
			<div class="samepost clear">
				<h2><a href="post.php?id=<?= $post['id']; ?>" ><?= $post['title']; ?></a></h2>
				<h4><?=  $query->time_formate($post['date']); ?>, By <a href="#"><?= $post['author']; ?></a></h4>
				 <a href="#"><img src="data:image;base64,<?= $post['image']; ?>" alt="post image"/></a>

                <?= $query->textShort($post['body']) ; ?>

				<div class="readmore clear">
					<a href="post.php?id=<?= $post['id']; ?>"  >Read More</a>
				</div>
			</div>
            <?php endforeach ?>
            <style>
                .pagination{
                    margin-left: 230px;
                }
                .pagination a{
                    background: #E6AF4B;
                    color: #fff;
                    font-weight: 700;
                    font-size: 18px;
                    padding: 2px 5px 2px 5px;
                    text-decoration: none;
                    cursor: pointer;
                }
                .pagination li{
                    display: inline-block;
                    text-align: center;
                    margin: 3px;
                }
                }
            </style>

            <div class="pagination">
            <?php

            $pageNext = $page_number + 1;
            $pagePevious = $page_number-1;

            if ($page_number > 1){
                echo "<li><a href='index.php?page = $pagePevious'>". "Previous". "</li></a>";
            }

            if($pages > 1) {
                for ($i = 1; $i <= $pages; $i++) {
                    if ($i == $page_number){
                        echo "<li><a style='color: #f5493e'>" . $i . "</li></a>";
                    }
                    else{
                        echo "<li><a href='index.php?page=$i'>".  $i ."</li></a>";
                    }
                }
            }

            if ($page_number < $pages){
                echo "<li><a href='index.php?page=$pageNext'>" . "Next" . "</li></a>";
            }


            ?>

        </div>
		</div>

        <?php
        include_once $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."views".DIRECTORY_SEPARATOR."elements".DIRECTORY_SEPARATOR."sideber.php";

        ?>

	</div>


<?php
include_once $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."views".DIRECTORY_SEPARATOR."elements".DIRECTORY_SEPARATOR."footer.php";
?>



