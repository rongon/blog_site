<?php
include_once $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."views".DIRECTORY_SEPARATOR."elements".DIRECTORY_SEPARATOR."header.php";
?>

	<div class="contentsection contemplete clear">
		<div class="maincontent clear">
			<div class="about">
				<div class="notfound">
    				<p><span>404</span> Not Found</p>
    			</div>
	        </div>
		</div>
        <?php
        include_once $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."views".DIRECTORY_SEPARATOR."elements".DIRECTORY_SEPARATOR."sideber.php";
        ?>
	</div>
<?php
include_once $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."views".DIRECTORY_SEPARATOR."elements".DIRECTORY_SEPARATOR."footer.php";
?>