<?php
include_once $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR
    . "autoload.php";

include_once $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."views".DIRECTORY_SEPARATOR."elements".DIRECTORY_SEPARATOR."header.php";

include_once $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."views".DIRECTORY_SEPARATOR."elements".DIRECTORY_SEPARATOR."slider.php";
use rongon\query\query;
use rongon\query\pagination;
$query = new Query();
$pagination = new pagination();

$scarch = $query->scarch($_POST['scarch']);
?>

<div class="contentsection contemplete clear">

    <div class="maincontent clear">


        <?php
        foreach ($scarch as $scarc) : ?>
            <div class="samepost clear">
                <h2><a href="post.php?id=<?= $post['id']; ?>" ><?= $scarc['title']; ?></a></h2>
                <h4><?=  $query->time_formate($scarc['date']); ?>, By <a href="#"><?= $scarc['author']; ?></a></h4>
                <a href="#"><img src="data:image;base64,<?= $scarc['image']; ?>" alt="post image"/></a>

                <?= $query->textShort($scarc['body']) ; ?>

                <div class="readmore clear">
                    <a href="post.php?id=<?= $scarc['id']; ?>"  >Read More</a>
                </div>
            </div>
        <?php endforeach ?>
    </div>

    <?php
    include_once $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."views".DIRECTORY_SEPARATOR."elements".DIRECTORY_SEPARATOR."sideber.php";

    ?>

</div>


<?php
include_once $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."views".DIRECTORY_SEPARATOR."elements".DIRECTORY_SEPARATOR."footer.php";
?>



