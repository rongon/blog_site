<?php
include_once $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR
    . "autoload.php";
include_once $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."views".DIRECTORY_SEPARATOR."elements".DIRECTORY_SEPARATOR."header.php";
include_once $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."views".DIRECTORY_SEPARATOR."elements".DIRECTORY_SEPARATOR."slider.php";
use rongon\query\query;
$query = new query();
$category = $_GET['category'];
$posts =$query->related_post($category);
?>
<div class="contentsection contemplete clear">
    <div class="maincontent clear">
        <?php
        foreach ($posts as $post) : ?>
            <div class="samepost clear">
                <h2><a href="post.php?id=<?= $post['id']; ?>" ><?= $post['title']; ?></a></h2>
                <h4><?=  $query->time_formate($post['date']); ?>, By <a href="#"><?= $post['author']; ?></a></h4>
                <a href="#"><img src="data:image;base64,<?= $post['image']; ?>" alt="post image"/></a>
                <?= $query->textShort($post['body']) ; ?>
                <div class="readmore clear">
                    <a href="post.php?id=<?= $post['id']; ?>"  >Read More</a>
                </div>
            </div>
        <?php endforeach ?>
    </div>
    <?php include_once $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."views".DIRECTORY_SEPARATOR."elements".DIRECTORY_SEPARATOR."sideber.php"; ?>
</div>
<?php
include_once $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."views".DIRECTORY_SEPARATOR."elements".DIRECTORY_SEPARATOR."footer.php";
?>


