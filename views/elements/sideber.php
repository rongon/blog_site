<?php
include_once $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR
    . "autoload.php";

use rongon\query\query;
use rongon\admin\catagori;
$query = new query();
$cat = new catagori();
$categorys = $cat->category();
$posts = $query->latest();

?>

<div class="sidebar clear">
    <div class="samesidebar clear">
        <h2>Categories</h2>
        <ul>
            <?php
                foreach ($categorys as $category):
            ?>
            <li><a href="posts.php?category=<?= $category['cat']?>"><?= $category['name']?></a></li>
            <?php endforeach;?>
        </ul>
    </div>

    <div class="samesidebar clear">
        <h2>Latest articles</h2>
        <?php
            foreach ($posts as $post):
        ?>
        <div class="popular clear">
            <h3><a href="post.php?id=<?= $post['id']?>"><?= $post['title']?></a></h3>
            <a href="#"><img src="data:image;base64,<?= $post['image']?>" alt="post image"/></a>
            <?= $query->textShort($post['body'],150) ; ?>
        </div>
        <?php endforeach;?>
    </div>

</div>