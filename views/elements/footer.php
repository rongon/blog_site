<?php
include_once $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR
    . "autoload.php";
use rongon\admin\socal;
$query = new socal();
$copy = $query->copyright();


    ?>


<div class="footersection templete clear">
    <div class="footermenu clear">
        <ul>
            <li><a href="#">Home</a></li>
            <li><a href="#">About</a></li>
            <li><a href="#">Contact</a></li>
            <li><a href="#">Privacy</a></li>
        </ul>
    </div>
    <p>&copy;  <?= $copy['copyright']?> <?= date('Y')?> </p>
</div>
<div class="fixedicon clear">
    <a href="http://www.facebook.com"><img src="/asstes/images/fb.png" alt="Facebook"/></a>
    <a href="http://www.twitter.com"><img src="/asstes/images/tw.png" alt="Twitter"/></a>
    <a href="http://www.linkedin.com"><img src="/asstes/images/in.png" alt="LinkedIn"/></a>
    <a href="http://www.google.com"><img src="/asstes/images/gl.png" alt="GooglePlus"/></a>
</div>
<script type="text/javascript" src="/asstes/js/scrolltop.js"></script>