<html>
<head>
    <title>Basic Website</title>
    <meta name="language" content="English">
    <meta name="description" content="It is a website about education">
    <meta name="keywords" content="blog,cms blog">
    <meta name="author" content="Delowar">
    <link rel="stylesheet" href="/asstes/font-awesome-4.5.0/css/font-awesome.css">
    <link rel="stylesheet" href="/asstes/css/nivo-slider.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="/asstes/css/style.css">
    <script src="/asstes/js/jquery.js" type="text/javascript"></script>
    <script src="/asstes/js/jquery.nivo.slider.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(window).load(function() {
            $('#slider').nivoSlider({
                effect:'random',
                slices:10,
                animSpeed:500,
                pauseTime:5000,
                startSlide:0, //Set starting Slide (0 index)
                directionNav:false,
                directionNavHide:false, //Only show on hover
                controlNav:false, //1,2,3...
                controlNavThumbs:false, //Use thumbnails for Control Nav
                pauseOnHover:true, //Stop animation while hovering
                manualAdvance:false, //Force manual transitions
                captionOpacity:0.8, //Universal caption opacity
                beforeChange: function(){},
                afterChange: function(){},
                slideshowEnd: function(){} //Triggers after all slides have been shown
            });
        });
    </script>
</head>

<body>
<?php
include_once $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR
    . "autoload.php";
use rongon\admin\slogan;
use rongon\admin\socal;
$socal = new socal();
$query = new slogan();
$slogan = $query->show();
$link = $socal->socal();


?>
<div class="headersection templete clear">
    <a href="#">
        <div class="logo">
            <img src="data:image;base64,<?= $slogan['logo']?>" alt="Logo"/>
            <h2><?= $slogan['title']?></h2>
            <p><?= $slogan['slogan']?></p>
        </div>
    </a>
    <div class="social clear">
        <div class="icon clear">
            <a href="<?= $link['facebook']?>" target="_blank"><i class="fa fa-facebook"></i></a>
            <a href="<?= $link['instragram']?>" target="_blank"><i class="fa fa-instagram"></i></a>
        </div>
    </div>
        <div class="searchbtn clear">
            <form action="/views/pages/scarch.php" method="post">
                <input type="text" name="scarch" placeholder="Search keyword..."/>
                <input type="submit" name="submit" value="Search"/>
            </form>
        </div>

</div>
<div class="navsection templete">
    <ul>
        <li><a  href="/views/pages/index.php">Home</a></li>
        <li><a href="/views/pages/about.php">About</a></li>
        <li><a href="/views/pages/contact.php">Contact</a></li>
    </ul>
</div>